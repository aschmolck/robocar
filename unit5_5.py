# -----------
# User Instructions
#
# Define a function smooth that takes a path as its input
# (with optional parameters for weight_data, weight_smooth)
# and returns a smooth path.
#
# Smoothing should be implemented by iteratively updating
# each entry in newpath until some desired level of accuracy
# is reached. The update should be done according to the
# gradient descent equations given in the previous video:
#
# If your function isn't submitting it is possible that the
# runtime is too long. Try sacrificing accuracy for speed.
# -----------


from math import *

# Don't modify path inside your function.
path = [[0, 0],
        [0, 1],
        [0, 2],
        [1, 2],
        [2, 2],
        [3, 2],
        [4, 2],
        [4, 3],
        [4, 4]]

# ------------------------------------------------
# smooth coordinates
#
def dot(a,b):
    return sum(x*y for x,y in zip(a,b))

def add(a,b):
    return [x+y for x,y in zip(a,b)]

def scale(alpha, b):
    return [alpha * y for y in b]

def sqnorm(a):
    return dot(a, a)

def smooth(path, weight_data = 0.5, weight_smooth = 0.1):

    # Make a deep copy of path into newpath
    newpath = [[0 for col in range(len(path[0]))] for row in range(len(path))]
    for i in range(len(path)):
        for j in range(len(path[0])):
            newpath[i][j] = path[i][j]


    #### ENTER CODE BELOW THIS LINE ###

    tol = 0.0000005
    alpha, beta, x, y = weight_data, weight_smooth, path, newpath
    while 1:
        y_new = [add(add(y[i],
                         scale(alpha, add(x[i], scale(-1, y[i])))),
                     scale(beta, add(add(y[i-1], y[i+1]), scale(-2, y[i]))))
                 for i in xrange(1, len(y)-1)]
        budge = max((x_i - y_i)**2
                    for xs, ys in zip(y[1:-1], y_new)
                    for x_i, y_i in zip(xs,ys))
        print budge, y_new
        if budge < tol**2:
            return y
        else:
            y[1:-1] = y_new

def smooth(path, weight_data = 0.5, weight_smooth = 0.1, tolerance = 0.00001):
    tolerance  = 0.00001
    change = tolerance
    newpath = [complex(*x) for x in path]
    path = newpath[:]
    while (change >= tolerance):
        change = 0.0
        for i in range(1, len(path)-1):
            aux = newpath[i]
            newpath[i] = newpath[i] + weight_data * (path[i] - newpath[i])
            newpath[i] = newpath[i] + weight_smooth * (newpath[i-1] + newpath[i+1] - (2.0 * newpath[i]))
            change += abs(aux - newpath[i])
    return [(x.real, x.imag) for x in newpath]

def smooth2(path, weight_data = 0.5, weight_smooth = 0.1, tolerance = 0.00001):
    tolerance  = 0.00001
    change = tolerance
    x = [complex(*x) for x in path]
    y = x[:]
    alpha, beta = weight_data, weight_smooth
    while (change >= tolerance):
        change = 0.0
        for i in range(1, len(x)-1):
            aux = y[i]
            y[i] += alpha * (x[i] - y[i])
            y[i] += beta  * (y[i-1] + y[i+1] - (2.0 * y[i]))
            change += abs(aux - y[i])
    return list((x.real, x.imag) for x in y)




# feel free to leave this and the following lines if you want to print.
newpath = smooth(path)

# thank you - EnTerr - for posting this on our discussion forum
for i in range(len(path)):
    print '['+ ', '.join('%.3f'%x for x in path[i]) +'] -> ['+ ', '.join('%.3f'%x for x in newpath[i]) +']'






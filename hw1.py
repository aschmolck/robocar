# pylint: disable=C0103,C0324,C0301
colors = [['red', 'green', 'green', 'red' , 'red'],
          ['red', 'red', 'green', 'red', 'red'],
          ['red', 'red', 'green', 'green', 'red'],
          ['red', 'red', 'red', 'red', 'red']]

measurements = ['green', 'green', 'green' ,'green', 'green']


motions = [[0,0],[0,1],[1,0],[1,0],[0,1]]

sensor_right = 0.7

p_move = 0.8


colors = [['green']*3, ['green', 'red', 'red'], ['green']*3]
measurements = ['red', 'red']
motions = [[0, 0], [0, 1]]

sensor_right = 1.0
p_move = 1.0


def show(p):
    for i in range(len(p)):
        print p[i]

#DO NOT USE IMPORT
#ENTER CODE BELOW HERE
#ANY CODE ABOVE WILL CAUSE
#HOMEWORK TO BE GRADED
#INCORRECT

def map2i(f, matrix):
    return [[f(i,j) for j in range(len(matrix[i]))]
            for i in range(len(matrix))]

def normalize(p_u):
    m, n = map(len, [p_u, p_u[0]])
    normalizer = float(sum(p_u[i][j] for i in range(m) for j in range(n)))
    return map2i(lambda i,j: p_u[i][j]/normalizer, p_u)

def sense(p, measurement, colors=colors, sensor_right=sensor_right):
    sensor_wrong = 1-sensor_right
    return normalize(map2i(
        lambda i,j: p[i][j]*(sensor_right if colors[i][j] == measurement
                             else sensor_wrong),
        colors))


def move(p, motion, p_move=p_move):
    p_no_move = 1-p_move
    m, n = map(len, [p, p[0]])
    return normalize(map2i(lambda i,j:
                           p_no_move * p[i][j] +
                           p_move    * p[(i-motion[0])%m][(j-motion[1])%n],
                           p))

p0 = normalize(map2i(lambda i,j: 1.0, colors))
# pylint: disable=R0913
def do(p=p0, motions=motions, measurements=measurements, # pylint: disable=W0102
       colors=colors, sensor_right=sensor_right, p_move=p_move): # pylint: disable=R0913
    return normalize(reduce(lambda p, (measurement, motion):
                            sense(move(p, motion, p_move),
                                  measurement, colors, sensor_right),
                            zip(measurements, motions),
                            p))



p = do()







#Your probability array must be printed
#with the following code.

show(p)

# ----------
# User Instructions:
#
# Create a function compute_value() which returns
# a grid of values. Value is defined as the minimum
# number of moves required to get from a cell to the
# goal.
#
# If it is impossible to reach the goal from a cell
# you should assign that cell a value of 99.

# ----------

grid = [[0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0]]

init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1]

delta = [[-1, 0 ], # go up
         [ 0, -1], # go left
         [ 1, 0 ], # go down
         [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>']

cost_step = 1 # the cost associated with moving from a cell to an adjacent one.

# ----------------------------------------
# insert code below
# ----------------------------------------
impossible = 99
def admissible((x,y)):
    def check_delta((xd, yd)):
        xt, yt = x+xd, y+yd
        return 0<=xt<len(grid) and 0<=yt<len(grid[0]) and value[x][y] != -1
    return check_delta

gos = 10
def fill_in_value((x, y)):
    global gos
    gos-=1
    if gos == 0: BREAKME

    pprint.pprint(value)
    print x, y
    if value[x][y] != -1:
        return value[x][y]
    else:
        if [x, y] == goal:
            ans = 0
        else:
            admissible_deltas = filter(admissible([x, y]), delta)
            if not admissible_deltas:
                ans = impossible
            else:
                ans = min(cost_step + fill_in_value([x+xd, y+yd])
                          for (xd, yd) in admissible_deltas)
        value[x][y] = ans
        return ans

def compute_value():
    global value
    value = [[impossible if grid[x][y] else -1
              for y in range(len(grid[0]))]
             for x in range(len(grid))]
    fill_in_value([goal[0], goal[1]])

    return value #make sure your function returns a grid of values as demonstrated in the previous video.


import pprint
pprint.pprint(compute_value())

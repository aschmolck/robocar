# -----------
# User Instructions:
#
# Modify the function search() so that it returns
# a table of values called expand. This table
# will keep track of which step each node was
# expanded.
#
# For grading purposes, please leave the return
# statement at the bottom.
# ----------


grid = [[0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 1, 0],
        [0, 0, 1, 0, 1, 0],
        [0, 0, 1, 0, 1, 0]]

init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1]

delta = [[-1, 0 ], # go up
         [ 0, -1], # go left
         [ 1, 0 ], # go down
         [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>']

cost = 1


# ----------------------------------------
# modify code below
# ----------------------------------------

def delta_cost((x,y), expand):
    def cost((xd, yd)):
        xt, yt = x+xd, y+yd
        return (2**31 if not (0<=xt<len(grid) and 0<=yt<len(grid[0]))
                or expand[xt][yt] == -1 else expand[xt][yt])
    return cost

def search():
    closed = [[0 for row in range(len(grid[0]))] for col in range(len(grid))]
    closed[init[0]][init[1]] = 1
    expand = [[-1 for row in range(len(grid[0]))] for col in range(len(grid))]
    expand[init[0]][init[1]] = 0

    x = init[0]
    y = init[1]
    g = 0

    open = [[g, x, y]]

    found = False  # flag that is set when search is complete
    resign = False # flag set if we can't find expand

    step = 0
    while not found and not resign:
        if len(open) == 0:
            resign = True
        else:
            open.sort()
            open.reverse()
            next = open.pop()
            x = next[1]
            y = next[2]
            g = next[0]
            expand[x][y] = step
            step +=1
            if x == goal[0] and y == goal[1]:
                found = True
            else:
                for i in range(len(delta)):
                    x2 = x + delta[i][0]
                    y2 = y + delta[i][1]
                    if x2 >= 0 and x2 < len(grid) and y2 >=0 and y2 < len(grid[0]):
                        if closed[x2][y2] == 0 and grid[x2][y2] == 0:
                            g2 = g + cost
                            open.append([g2, x2, y2])
                            closed[x2][y2] = 1



    pos = goal
    path = [[' '] * len(grid[1]) for _ in grid]
    path[goal[0]][goal[1]] = '*'
    for i in range(30):
        delta_t = min(delta, key=delta_cost(pos, expand))
        print delta_t, pos
        pos = map(sum, zip(pos, delta_t))
        path[pos[0]][pos[1]] = delta_name[delta.index([-a for a in delta_t])]
        if pos == init:
            break
    print numpy.array(expand)
    return path

import pprint
import numpy
print pprint.pprint(numpy.array(search()))


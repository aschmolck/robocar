# ----------
# User Instructions:
#
# Define a function, search() that takes no input
# and returns a list
# in the form of [optimal path length, x, y]. For
# the grid shown below, your function should output
# [11, 4, 5].
#
# If there is no valid path from the start point
# to the goal, your function should return the string
# 'fail'
# ----------

# Grid format:
#   0 = Navigable space
#   1 = Occupied space

grid = [[0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 0]]

init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1] # Make sure that the goal definition stays in the function.

delta = [[-1, 0 ], # go up
        [ 0, -1], # go left
        [ 1, 0 ], # go down
        [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>']

cost = 1

max_x = len(grid[0])-1
max_y = len(grid)-1

def expand((last_cost, y, x)):
    gen = ((last_cost + cost, y+yd, x+xd) for (yd, xd) in delta)
    return filter((lambda (_, y,x):
                   0 <= x <= max_x and
                   0 <= y <= max_y and not grid[y][x]), gen)

def search():
    # should use a heap
    states = [(0, 0, 0)]
    seen = set([(0, 0)])
    while states:
        ## import pprint
        ## print 'STATES'
        ## pprint.pprint(states)
        next_state = min(states)
        if list(next_state[1:]) == goal:
            return list(next_state)
        states.remove(next_state)
        new_states = expand(next_state)
        states.extend([n for n in new_states if n[1:] not in seen])
        seen |= set(n[1:] for n in new_states)

    raise RuntimeError('goal not found')

if __name__ == '__main__':
    print search()

import numpy as np
import random
a = np.array([[5.,3.],
              [3.,1.]])
def step(a):
    a2 = a.copy()
    for i in range(2):
        for j in range(2):
            if a[i,j]:
                a2[i,j] -= 1
                if random.random() < .5:
                    a2[not i, j] += 1
                else:
                    a2[i, not j] +=1
    assert a2.sum() == 12
    return a2



